import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from './../../src/app.module';
import { INestApplication } from '@nestjs/common';
import { WidgetModule } from './../../src/widget/widget.module';
import { AppService } from './../../src/app.service';
import { ConfigService } from '@nestjs/config';
import { WidgetService } from './../../src/widget/widget.service';

describe('WidgetController (e2e)', () => {
  let app: INestApplication, widgetService: WidgetService;
  let basePath;
  const mockConfigService = {
    get(key) {
      if ('MONGO' === key) {
        return process.env.MONGO_E2E;
      }
    },
  };
  beforeEach(async () => {
    basePath = '/widgets/';
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule, WidgetModule],
      providers: [AppService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    widgetService = app.get(WidgetService);
    await app.init();
  });
  afterEach(async () => {
    await widgetService['widgetModel'].collection.deleteMany({});
  });
  afterAll(async () => {
    await app.close();
  });

  describe('/widgets/1 (GET)', () => {
    let mockId: string, widgetToRetrieve;
    beforeEach(async () => {
      mockId = '61148300a5cf12508c51e2e7';
      widgetToRetrieve = {
        _id: mockId,
        name: 'a widget to retrieve',
        type: 'mock',
        code: 2,
      };
      // setup db with a widget to test with
      await request(app.getHttpServer())
        .post(basePath)
        .send(widgetToRetrieve)
        .expect(201)
        .expect({ ...widgetToRetrieve, __v: 0 });
    });
    test(`should return expected body and status`, () => {
      return request(app.getHttpServer())
        .get(basePath + mockId)
        .expect(200)
        .expect({ ...widgetToRetrieve, __v: 0 });
    });
  });
  describe('/widgets/ (POST)', () => {
    test(`should return expected body and status`, async () => {
      const mockWidgetToPost = {
        _id: '61148300a5cf12508c51e2e7',
        name: 'mock widget 2',
        type: 'mock',
        code: 2,
      };
      const postResponse = await request(app.getHttpServer())
        .post(basePath)
        .send(mockWidgetToPost)
        .expect(201)
        .expect({ ...mockWidgetToPost, __v: 0 });
      return await request(app.getHttpServer())
        .get(basePath + postResponse.body._id)
        .expect(200)
        .expect({ ...mockWidgetToPost, __v: 0 });
    });
  });
});
