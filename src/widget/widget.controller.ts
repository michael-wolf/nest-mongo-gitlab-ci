import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Widget } from './dto/widget.dto';
import { WidgetService } from './widget.service';

@Controller('widgets')
export class WidgetController {
  constructor(private widgetService: WidgetService) {}

  @Get(':id')
  async getById(@Param('id') id: string): Promise<Widget> {
    return await this.widgetService.findOneById(id);
  }

  @Post()
  async create(@Body() widget: Widget): Promise<Widget> {
    return await this.widgetService.create(widget);
  }
}
