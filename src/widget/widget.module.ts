import { Module } from '@nestjs/common';
import { WidgetController } from './widget.controller';
import { WidgetService } from './widget.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Widget } from './dto/widget.dto';

@Module({
  imports: [
    TypegooseModule.forFeature([
      { typegooseClass: Widget, schemaOptions: { collection: 'widgets' } },
    ]),
  ],
  controllers: [WidgetController],
  providers: [WidgetService],
})
export class WidgetModule {}
