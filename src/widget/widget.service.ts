import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Widget } from './dto/widget.dto';

@Injectable()
export class WidgetService {
  constructor(
    @InjectModel(Widget)
    private readonly widgetModel: ReturnModelType<typeof Widget>,
  ) {}

  async create(createWidgetDto: Widget): Promise<Widget> {
    return await this.widgetModel.create(createWidgetDto);
  }

  async findOneById(id): Promise<Widget> {
    return this.widgetModel.findOne({ _id: id }).exec();
  }
}
