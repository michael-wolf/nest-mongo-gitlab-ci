import { prop } from '@typegoose/typegoose';

export class Widget {
  @prop({ required: false })
  id?: string;

  @prop()
  name: string;

  @prop()
  type: string;

  @prop()
  code: number;

  @prop({ required: false })
  __v?: number;
}
