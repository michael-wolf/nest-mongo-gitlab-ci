/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Test, TestingModule } from '@nestjs/testing';
import { Widget } from './dto/widget.dto';
import { WidgetController } from './widget.controller';
import { WidgetService } from './widget.service';

describe('WidgetController', () => {
  let app: TestingModule,
    widgetController: WidgetController,
    widgetService: WidgetService;
  class MockWidgetService {
    findOneById() {}
    create() {}
  }
  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [WidgetController],
      providers: [{ provide: WidgetService, useClass: MockWidgetService }],
    }).compile();
    widgetController = app.get<WidgetController>(WidgetController);
    widgetService = app.get<WidgetService>(WidgetService);
  });

  describe('getById', () => {
    let findOneByIdSpy, mockWidget;

    beforeAll(async () => {
      mockWidget = { _id: '123456', name: 'mock', code: 0, type: 'test' };
      findOneByIdSpy = jest
        .spyOn(widgetService, 'findOneById')
        .mockImplementation(() => Promise.resolve(mockWidget));
    });

    it('should return expected widget"', async () => {
      const expected: Widget = mockWidget;
      expect(await widgetController.getById('123')).toEqual(expected);
    });
  });
  describe('getById', () => {
    let createSpy, mockWidget;

    beforeAll(async () => {
      mockWidget = { _id: '123456', name: 'mock', code: 0, type: 'test' };
      createSpy = jest
        .spyOn(widgetService, 'create')
        .mockImplementation(() => Promise.resolve(mockWidget));
    });

    it('should return expected widget"', async () => {
      const mockWidgetToCreate = { name: 'mock widget', type: 'mock', code: 0 };
      expect(await widgetController.create(mockWidgetToCreate)).toBe(
        mockWidget,
      );
    });
  });
});
