import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();
  });

  describe('getHello', () => {
    it('should return correct health message', () => {
      const appController: AppController =
        app.get<AppController>(AppController);
      const expected = 'GitLab CI Demo App Running!';
      expect(appController.getHello()).toBe(expected);
    });
  });
});
